# Nes Wirelss Controller

Nintendo entertainment System -  wireless controller mod

# Parts - Transmitters

* NES controller: NES-004
* NRF24l01
* CX-10 3.7V 100mAh
* 3.3voltage regulator
* usb c lipo charger
* Atmel ATTiny24
* 2 blue leds for player indicator
* red led for charge indicator
* power button to fully shutdown


# Parts - receiver

* Atmel ATTiny24 - connected to both controller ports
* NRF24l01

# Features
- sync: first controller on should always be player 1
- auto standby: toggle low power mode and disconnect from nes when controller not in use for 10 minutes
- send shutoff command to controllers when NES is powered down.

